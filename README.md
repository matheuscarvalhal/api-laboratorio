## Api laboratório

Esta é uma implementação simples de uma API para manutenção de laboratórios e exames.

Este projeto é uma implementação feito no contêiner Docker:

- Back-end Java (Spring Boot)

O ponto de entrada para um usuário é um site que está disponível no
endereço: **http://localhost:8080/api/**   ou **http://192.168.99.100:8080/api/**

! [Scrum] (https://github.com/techtter/scrum-board/blob/master/assets/scrum.gif)

---

## Arquitetura Java

A aplicação usa Spring Boot.

E o código se organiza em 3 camadas:

- common, responsável pelas as entidades, negócios e classes de configuração.
- services, responsável pelo serviços de alto nível para consulta com os objetos de transferência de dados.
- controller, responsável pelo o recebimento das requisições do client.

---

### Pré-requisitos

Para executar este aplicativo, você precisa instalar duas ferramentas: ** Docker ** e ** Docker Compose **.

Instruções de como instalar o ** Docker ** no [Ubuntu] (https://docs.docker.com/install/linux/docker-ce/ubuntu/), [Windows] (https://docs.docker.com/ docker-for-windows / install /), [Mac] (https://docs.docker.com/docker-for-mac/install/).

** Dosker Compose ** já está incluído nos pacotes de instalação para * Windows * e * Mac *, portanto, apenas usuários do Ubuntu precisam seguir [estas instruções] (https://docs.docker.com/compose/install/).



### Como executar?

API pode ser executado com um único comando em um terminal:

```
$ docker-compose up -d
```

Se quiser pará-lo, use o seguinte comando:

```
$ docker-compose down
```


---


#### laboratorio-api (REST API)

Este é um aplicativo baseado em Spring Boot (Java), Expõe os pontos de extremidade REST que podem ser consumidos por pelo swagger
Ele suporta vários métodos HTTP REST como GET, POST, PUT e DELETE.

A lista completa de endpoints REST disponíveis pode ser encontrada na IU Swagger,
que pode ser chamado usando o link:  **http://localhost:8080/api/swagger-ui.html**  ou  **http://192.168.99.100:8080/api/swagger-ui.html **


![swagger-ui](https://github.com/techtter/scrum-board/blob/master/assets/swagger.png)


Este aplicativo também é colocado no contêiner do Docker e sua definição pode ser encontrada
em um arquivo * scrum-app / Dockerfile *.


