package br.com.api.laboratorio;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.api.laboratorio.common.business.ExameService;
import br.com.api.laboratorio.common.entity.Exame;
import br.com.api.laboratorio.common.enums.TipoExame;

@RunWith(MockitoJUnitRunner.class)
class LaboratorioApplicationTests {

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getAllExamesAtivoTest() {

		ExameService exameServiceMock = mock(ExameService.class);

		Set<Exame> list = new HashSet<Exame>();
		Exame analise = new Exame("Analise", TipoExame.ANALISE);
		Exame imagem = new Exame("Imagem", TipoExame.IMAGEM);
		Exame clinica = new Exame("Clinica", TipoExame.CLINICA);

		list.add(analise);
		list.add(imagem);
		list.add(clinica);

		when(exameServiceMock.buscarTodosAtivo()).thenReturn(list);

		Assert.assertEquals(exameServiceMock.buscarTodosAtivo(), list);
	}

	@Test
	public void createExameTest() {

		ExameService exameServiceMock = mock(ExameService.class);

		Exame analise = new Exame("Imagem", TipoExame.IMAGEM);

		when(exameServiceMock.save(Mockito.any(Exame.class))).thenReturn(analise);

		Assert.assertEquals(exameServiceMock.save(analise), analise);
	}

	@Test
	public void updateExameTest() {

		ExameService exameServiceMock = mock(ExameService.class);

		Exame analise = new Exame("Clinica", TipoExame.CLINICA);

		when(exameServiceMock.update(Mockito.any(Exame.class))).thenReturn(analise);

		Assert.assertEquals(exameServiceMock.update(analise), analise);
	}

	@Test
	public void findByIdExameTest() {

		ExameService exameServiceMock = mock(ExameService.class);

		Exame clinica = new Exame("Clinica", TipoExame.CLINICA);
		clinica.setId(1L);

		when(exameServiceMock.findById(Mockito.any(Long.class))).thenReturn(clinica);

		Assert.assertEquals(exameServiceMock.findById(Mockito.any(Long.class)), clinica);
	}

}
