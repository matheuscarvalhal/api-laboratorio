package br.com.api.laboratorio.common.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import br.com.api.laboratorio.common.enums.Status;
import br.com.api.laboratorio.common.enums.TipoExame;

public class Exame {

	private Long id;
	private String nome;
	private TipoExame tipo;
	private Status status;
	private List<LaboratorioExame> laboratorios = new ArrayList<LaboratorioExame>();

	public Exame() {
	}

	public Exame(String nome, TipoExame tipo) {
		super();
		this.nome = nome;
		this.tipo = tipo;
		this.status = Status.ATIVO; 
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public TipoExame getTipo() {
		return tipo;
	}

	public void setTipo(TipoExame tipo) {
		this.tipo = tipo;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public List<LaboratorioExame> getLaboratorios() {
		return laboratorios;
	}

	public void setLaboratorios(List<LaboratorioExame> laboratorios) {
		this.laboratorios = laboratorios;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, nome, tipo);
	}

}