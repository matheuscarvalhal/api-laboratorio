package br.com.api.laboratorio.common.entity;

public class LaboratorioExame {

	private Laboratorio laboratorio;
	private Exame exame;
	
	public LaboratorioExame() {
		super();
	}
	
	public LaboratorioExame(Laboratorio laboratorio, Exame exame) {
		super();
		this.laboratorio = laboratorio;
		this.exame = exame;
	}

	public Laboratorio getLaboratorio() {
		return laboratorio;
	}

	public void setLaboratorio(Laboratorio laboratorio) {
		this.laboratorio = laboratorio;
	}

	public Exame getExame() {
		return exame;
	}

	public void setExame(Exame exame) {
		this.exame = exame;
	}

}