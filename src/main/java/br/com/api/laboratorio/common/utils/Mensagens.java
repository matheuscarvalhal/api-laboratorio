package br.com.api.laboratorio.common.utils;

public class Mensagens {

	public static final String ERRO_ID_EXAME_NAO_ENCONTRADO = "Não foi encontrado o exame com ID %s.";
	public static final String ERRO_ID_LABORATORIO_NAO_ENCONTRADO = "Não foi encontrado o laborátorio com ID %s.";
	public static final String ERRO_EXAME_NAO_ENCONTRADO = "Não foi encontrado o exame informado.";
	public static final String ERRO_LABORATORIO_NAO_ENCONTRADO = "Não foi encontrado o laborátorio informado.";
	public static final String ERRO_EXAME_CADASTRADO = "Exame já cadastrado.";
	public static final String ERRO_LABORATORIO_CADASTRADO = "Laborátorio já cadastrado.";
	public static final String ERRO_CAMPOS_NAO_PREENCHIDOS = "Campos não preenchidos.";
}