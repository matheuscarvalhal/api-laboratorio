package br.com.api.laboratorio.common.business;

import java.util.List;
import java.util.Set;

public interface BaseService<T> {
	
	public static final String METODO_NAO_SUPORTA = "Método não implementado";

	Set<T> findAll();

	T findById(Long id);

	T save(T entity);
	
	List<T> saveAll(List<T> entities);

	T update(T entit);
	
	List<T> updateAll(List<T> entities);

	void delete(Long id);
	
	void deleteAll();
}