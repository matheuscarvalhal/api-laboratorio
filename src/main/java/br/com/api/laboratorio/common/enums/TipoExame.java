package br.com.api.laboratorio.common.enums;

public enum TipoExame {
	ANALISE, CLINICA, IMAGEM
}