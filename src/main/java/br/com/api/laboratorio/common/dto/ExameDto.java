package br.com.api.laboratorio.common.dto;

import br.com.api.laboratorio.common.enums.TipoExame;

public class ExameDto {
	private String nome;
	private TipoExame tipoExame;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public TipoExame getTipoExame() {
		return tipoExame;
	}

	public void setTipoExame(TipoExame tipoExame) {
		this.tipoExame = tipoExame;
	}
}
