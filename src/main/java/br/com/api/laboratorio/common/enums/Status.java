package br.com.api.laboratorio.common.enums;

public enum Status {
	ATIVO, INATIVO
}