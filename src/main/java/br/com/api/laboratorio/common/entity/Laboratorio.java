package br.com.api.laboratorio.common.entity;

import java.util.ArrayList;
import java.util.List;

import br.com.api.laboratorio.common.enums.Status;

public class Laboratorio {

	private Long id;
	private String nome;
	private String endereco;
	private Status status;
	private List<LaboratorioExame> exames = new ArrayList<LaboratorioExame>();

	public Laboratorio() {
	}

	public Laboratorio(String nome, String endereco) {
		super();
		this.nome = nome;
		this.endereco = endereco;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public List<LaboratorioExame> getExames() {
		return exames;
	}

	public void setExames(List<LaboratorioExame> exames) {
		this.exames = exames;
	}

}