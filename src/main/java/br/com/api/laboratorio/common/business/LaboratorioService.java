package br.com.api.laboratorio.common.business;

import java.util.List;
import java.util.Set;

import br.com.api.laboratorio.common.entity.Exame;
import br.com.api.laboratorio.common.entity.Laboratorio;

public interface LaboratorioService extends BaseService<Laboratorio> {
	
	Set<Laboratorio> buscarTodosAtivo();
	void associar(Exame exame, Laboratorio laboratorio);
	void desassociar(Exame exame, Laboratorio laboratorio);
	List<Laboratorio> buscarPorNomeExame(String nomeExame);
	
}