package br.com.api.laboratorio.common.business;

import java.util.Set;

import br.com.api.laboratorio.common.entity.Exame;
import br.com.api.laboratorio.common.entity.Laboratorio;

public interface ExameService extends BaseService<Exame> {
	
	Set<Exame> buscarTodosAtivo();
	void associar(Exame exame, Laboratorio laboratorio);
	void desassociar(Exame exame, Laboratorio laboratorio);
	
}