package br.com.api.laboratorio.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import br.com.api.laboratorio.common.business.LaboratorioService;
import br.com.api.laboratorio.common.entity.Exame;
import br.com.api.laboratorio.common.entity.Laboratorio;
import br.com.api.laboratorio.common.entity.LaboratorioExame;
import br.com.api.laboratorio.common.enums.Status;

@Service
public class LaboratorioServiceImpl implements LaboratorioService {

	private Set<Laboratorio> laboratorios = new HashSet<Laboratorio>();
	private Long sequence = 1L;

	@Override
	public Laboratorio save(Laboratorio novo) {
		Laboratorio antigo = laboratorios.stream()
				.filter(i -> i.getNome().equals(novo.getNome()) && i.getEndereco().equals(novo.getEndereco()))
				.findFirst().orElse(null);

		if (antigo == null) {
			novo.setId(sequence);
			novo.setStatus(Status.ATIVO);
			laboratorios.add(novo);
			sequence++;
			return novo;
		} else if (!antigo.getStatus().equals(Status.INATIVO)) {
			laboratorios.removeIf(i -> i.getNome().equals(novo.getNome()) && i.getEndereco().equals(novo.getEndereco()));
			antigo.setStatus(Status.ATIVO);
			laboratorios.add(antigo);
			return antigo;
		} else
			return null;
	}
	
	@Override
	public List<Laboratorio> saveAll(List<Laboratorio> entities) {
		entities.forEach(i -> {
			save(i);
		});
		return entities;
	}

	@Override
	public Laboratorio findById(Long id) {
		return laboratorios.stream().filter(i -> i.getId().equals(id) && i.getStatus().equals(Status.ATIVO)).findFirst()
				.orElse(null);
	}

	@Override
	public void delete(Long id) {
		Laboratorio laboratorio = findById(id);
		if (laboratorio != null)
			laboratorio.setStatus(Status.INATIVO);
	}
	
	@Override
	public void deleteAll() {
		laboratorios.forEach(i -> {
			if (i != null)
				i.setStatus(Status.INATIVO);
		});
	}

	@Override
	public Laboratorio update(Laboratorio entity) {
		if (entity.getId() == null)
			return null;
		laboratorios.removeIf(i -> i.getId().equals(entity.getId()));
		laboratorios.add(entity);
		return entity;
	}
	
	@Override
	public List<Laboratorio> updateAll(List<Laboratorio> entities) {
		entities.forEach(i -> {
			update(i);
		});
		return entities;
	}

	@Override
	public Set<Laboratorio> findAll() {
		return laboratorios;
	}

	@Override
	public Set<Laboratorio> buscarTodosAtivo() {
		return laboratorios.stream().filter(i -> i.getStatus().equals(Status.ATIVO)).collect(Collectors.toSet());
	}

	@Override
	public void associar(Exame exame, Laboratorio laboratorio) {
		
		laboratorios.removeIf(i -> i.getId().equals(laboratorio.getId()));
		List<LaboratorioExame> lista = laboratorio.getExames();
		lista.add(new LaboratorioExame(laboratorio, exame));
		laboratorio.setExames(lista);
		laboratorios.add(laboratorio);
		
	}

	@Override
	public void desassociar(Exame exame, Laboratorio laboratorio) {

		laboratorios.removeIf(i -> i.getId().equals(laboratorio.getId()));
		List<LaboratorioExame> lista = exame.getLaboratorios();
		lista.removeIf(i -> i.getLaboratorio().getId().equals(laboratorio.getId()) && i.getExame().getId().equals(exame.getId()));	
		laboratorio.setExames(lista);
		laboratorios.add(laboratorio);
		
	}

	@Override
	public List<Laboratorio> buscarPorNomeExame(String nomeExame) {
		List<Laboratorio> retorno = new ArrayList<>();
		laboratorios.forEach(l -> {
			l.getExames().forEach(e -> {
				if (e.getExame().getNome().equalsIgnoreCase(nomeExame))
					retorno.add(l);
			});
		});
		return retorno;
	}

}
