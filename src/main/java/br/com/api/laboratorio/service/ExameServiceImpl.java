package br.com.api.laboratorio.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import br.com.api.laboratorio.common.business.ExameService;
import br.com.api.laboratorio.common.entity.Exame;
import br.com.api.laboratorio.common.entity.Laboratorio;
import br.com.api.laboratorio.common.entity.LaboratorioExame;
import br.com.api.laboratorio.common.enums.Status;

@Service
public class ExameServiceImpl implements ExameService {

	private Set<Exame> exames = new HashSet<Exame>();
	private Long sequence = 1L;

	@Override
	public Exame save(Exame novo) {

		Exame antigo = exames.stream()
				.filter(i -> i.getNome().equals(novo.getNome()) && i.getTipo().equals(novo.getTipo())).findFirst()
				.orElse(null);

		if (antigo == null) {
			novo.setId(sequence);
			novo.setStatus(Status.ATIVO);
			exames.add(novo);
			sequence++;
			return novo;
		} else if (!antigo.getStatus().equals(Status.INATIVO)) {
			exames.removeIf(i -> i.getNome().equals(novo.getNome()) && i.getTipo().equals(novo.getTipo()));
			antigo.setStatus(Status.ATIVO);
			exames.add(antigo);
			return antigo;
		} else
			return null;
	}
	
	@Override
	public List<Exame> saveAll(List<Exame> entities) {
		entities.forEach(i -> {
			save(i);
		});
		return entities;
	}

	@Override
	public Exame findById(Long id) {
		return exames.stream().filter(i -> i.getId().equals(id) && i.getStatus().equals(Status.ATIVO)).findFirst().orElse(null);
	}

	@Override
	public void delete(Long id) {
		Exame exame = findById(id);
		if (exame != null)
			exame.setStatus(Status.INATIVO);
	}
	
	@Override
	public void deleteAll() {
		exames.forEach(i -> {
			i.setStatus(Status.INATIVO);
		});
	}

	@Override
	public Exame update(Exame entity) {
		if (entity.getId() == null)
			return null;
		exames.removeIf(i -> i.getId().equals(entity.getId()));
		exames.add(entity);
		return entity;
	}
	
	@Override
	public List<Exame> updateAll(List<Exame> entities) {
		entities.forEach(i -> {
			update(i);
		});
		return entities;
	}

	@Override
	public Set<Exame> findAll() {
		return exames;
	}

	@Override
	public Set<Exame> buscarTodosAtivo() {
		return exames.stream().filter(i -> i.getStatus().equals(Status.ATIVO)).collect(Collectors.toSet());
	}

	@Override
	public void associar(Exame exame, Laboratorio laboratorio) {
		
		exames.removeIf(i -> i.getId().equals(exame.getId()));
		List<LaboratorioExame> lista = exame.getLaboratorios();
		lista.add(new LaboratorioExame(laboratorio, exame));
		exame.setLaboratorios(lista);
		exames.add(exame);
		
	}

	@Override
	public void desassociar(Exame exame, Laboratorio laboratorio) {

		exames.removeIf(i -> i.getId().equals(exame.getId()));
		List<LaboratorioExame> laboratorios = exame.getLaboratorios();
		laboratorios.removeIf(i -> i.getLaboratorio().getId().equals(laboratorio.getId()) && i.getExame().getId().equals(exame.getId()));	
		exame.setLaboratorios(laboratorios);
		exames.add(exame);
		
	}
}
