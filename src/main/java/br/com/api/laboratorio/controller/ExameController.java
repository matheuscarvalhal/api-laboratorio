package br.com.api.laboratorio.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.api.laboratorio.common.business.ExameService;
import br.com.api.laboratorio.common.dto.ExameDto;
import br.com.api.laboratorio.common.entity.Exame;
import br.com.api.laboratorio.common.exceptions.ResourceNotFoundException;
import br.com.api.laboratorio.common.utils.Mensagens;
import io.swagger.annotations.ApiOperation;

@RestController
public class ExameController {

	@Autowired
	private ExameService exameService;

	@PostMapping("/exames")
	@ApiOperation(value = "Cadastra um exame.", httpMethod = "POST", response = Exame.class)
	public Exame cadastrar(@Valid @RequestBody ExameDto exameDto) throws ResourceNotFoundException {
		
		if(exameDto.getNome() == null || exameDto.getTipoExame() == null)
			throw new ResourceNotFoundException(Mensagens.ERRO_CAMPOS_NAO_PREENCHIDOS);
		
		Exame exame = exameService.save(new Exame(exameDto.getNome(), exameDto.getTipoExame()));

		if (exame == null)
			throw new ResourceNotFoundException(Mensagens.ERRO_EXAME_CADASTRADO);

		return exame;
	}
	
	@PostMapping("/examesEmLote")
	@ApiOperation(value = "Cadastra em lote exames.", httpMethod = "POST", response = List.class)
	public List<Exame> cadastrarEmLote(@Valid @RequestBody List<ExameDto> lista) {
		
		List<Exame> response = new ArrayList<Exame>();
		
		for (ExameDto dto : lista) {
			if(dto.getNome() == null || dto.getTipoExame() == null)
				continue;			
			response.add(new Exame(dto.getNome(), dto.getTipoExame()));
		}

		return exameService.saveAll(response);
	}

	@GetMapping("/exames")
	@ApiOperation(value = "Retorna todos os exames ativos.", httpMethod = "GET", response = Exame.class)
	public Set<Exame> listarAtivos() {
		return exameService.buscarTodosAtivo();
	}

	@PutMapping("/exames")
	@ApiOperation(value = "Atualiza um exame.", httpMethod = "PUT", response = Exame.class)
	public Exame atualizar(@RequestBody Exame exame) throws ResourceNotFoundException {
		if (exameService.update(exame) == null)
			throw new ResourceNotFoundException(Mensagens.ERRO_EXAME_NAO_ENCONTRADO);

		return exame;
	}
	
	@PutMapping("/examesEmLote")
	@ApiOperation(value = "Atualiza emn lote exames.", httpMethod = "PUT", response = List.class)
	public List<Exame> atualizarEmLote(@RequestBody List<Exame> lista) {
		
		lista.removeIf(i -> i.getId() == null || i.getNome() == null || i.getTipo() == null);

		return exameService.updateAll(lista);
	}

	@DeleteMapping("/exames/{id}")
	@ApiOperation(value = "Deleta o exame do ID informado.", httpMethod = "DELETE", response = Exame.class)
	public Map<String, Boolean> deletar(@PathVariable Long id) throws ResourceNotFoundException {
		Exame laboratorio = exameService.findById(id);

		if (laboratorio == null)
			throw new ResourceNotFoundException(String.format(Mensagens.ERRO_ID_EXAME_NAO_ENCONTRADO, id));

		exameService.delete(id);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deletado", Boolean.TRUE);
		return response;
	}
	
	@DeleteMapping("/examesEmLote")
	@ApiOperation(value = "Deleta todos os exames.", httpMethod = "DELETE", response = Map.class)
	public Map<String, Boolean> deletarEmLote() {
		exameService.deleteAll();
		Map<String, Boolean> response = new HashMap<>();
		response.put("deletado", Boolean.TRUE);
		return response;
	}

}
