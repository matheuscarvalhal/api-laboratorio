package br.com.api.laboratorio.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.api.laboratorio.common.business.ExameService;
import br.com.api.laboratorio.common.business.LaboratorioService;
import br.com.api.laboratorio.common.dto.LaboratorioExameDto;
import br.com.api.laboratorio.common.entity.Exame;
import br.com.api.laboratorio.common.entity.Laboratorio;
import br.com.api.laboratorio.common.exceptions.ResourceNotFoundException;
import br.com.api.laboratorio.common.utils.Mensagens;
import io.swagger.annotations.ApiOperation;

@RestController
public class AssociacaoController {

	@Autowired
	private ExameService exameService;

	@Autowired
	private LaboratorioService laboratorioService;

	@PostMapping("/associar")
	@ApiOperation(value = "Associar um exame ativo à um laboratório ativo", httpMethod = "POST", response = Laboratorio.class)
	public Map<String, Boolean> associar(@Valid @RequestBody LaboratorioExameDto dto) throws ResourceNotFoundException {

		if (dto.getIdExame() == null || dto.getIdLaboratorio() == null)
			throw new ResourceNotFoundException(Mensagens.ERRO_CAMPOS_NAO_PREENCHIDOS);

		Exame exame = exameService.findById(dto.getIdExame());
		if (exame == null)
			throw new ResourceNotFoundException(Mensagens.ERRO_EXAME_CADASTRADO);

		Laboratorio laboratorio = laboratorioService.findById(dto.getIdLaboratorio());
		if (laboratorio == null)
			throw new ResourceNotFoundException(Mensagens.ERRO_LABORATORIO_NAO_ENCONTRADO);

		exameService.associar(exame, laboratorio);
		laboratorioService.associar(exame, laboratorio);
		Map<String, Boolean> response = new HashMap<>();
		response.put("associado", Boolean.TRUE);
		return response;
	}

	@PostMapping("/desassociar")
	@ApiOperation(value = "Desassociar um exame ativo de um laboratório ativo.", httpMethod = "POST", response = Laboratorio.class)
	public Map<String, Boolean> desassociar(@Valid @RequestBody LaboratorioExameDto dto)
			throws ResourceNotFoundException {

		if (dto.getIdExame() == null || dto.getIdLaboratorio() == null)
			throw new ResourceNotFoundException(Mensagens.ERRO_CAMPOS_NAO_PREENCHIDOS);

		Exame exame = exameService.findById(dto.getIdExame());
		if (exame == null)
			throw new ResourceNotFoundException(Mensagens.ERRO_EXAME_CADASTRADO);

		Laboratorio laboratorio = laboratorioService.findById(dto.getIdLaboratorio());
		if (laboratorio == null)
			throw new ResourceNotFoundException(Mensagens.ERRO_LABORATORIO_NAO_ENCONTRADO);

		exameService.desassociar(exame, laboratorio);
		laboratorioService.desassociar(exame, laboratorio);
		Map<String, Boolean> response = new HashMap<>();
		response.put("desassociado", Boolean.TRUE);
		return response;
	}

}
