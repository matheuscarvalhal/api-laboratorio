package br.com.api.laboratorio.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.api.laboratorio.common.business.LaboratorioService;
import br.com.api.laboratorio.common.dto.LaboratorioDto;
import br.com.api.laboratorio.common.entity.Laboratorio;
import br.com.api.laboratorio.common.exceptions.ResourceNotFoundException;
import br.com.api.laboratorio.common.utils.Mensagens;
import io.swagger.annotations.ApiOperation;

@RestController
public class LaboratorioController {

	@Autowired
	private LaboratorioService laboratorioService;

	@PostMapping("/laboratorios")
	@ApiOperation(value = "Cadastra um laboratório.", httpMethod = "POST", response = Laboratorio.class)
	public Laboratorio cadastrar(@Valid @RequestBody LaboratorioDto laboratorioDto) throws ResourceNotFoundException {
		
		if(laboratorioDto.getNome() == null || laboratorioDto.getEndereco() == null)
			throw new ResourceNotFoundException(Mensagens.ERRO_CAMPOS_NAO_PREENCHIDOS);
		
		Laboratorio laboratorio = laboratorioService.save(new Laboratorio(laboratorioDto.getNome(), laboratorioDto.getEndereco()));

		if (laboratorio == null)
			throw new ResourceNotFoundException(Mensagens.ERRO_LABORATORIO_CADASTRADO);

		return laboratorio;
	}
	
	@PostMapping("/laboratoriosEmLote")
	@ApiOperation(value = "Cadastra em lote laboratórios.", httpMethod = "POST", response = List.class)
	public List<Laboratorio> cadastrarEmLote(@Valid @RequestBody List<LaboratorioDto> lista) {
		
		List<Laboratorio> response = new ArrayList<Laboratorio>();
		
		for (LaboratorioDto dto : lista) {
			if(dto.getNome() == null || dto.getEndereco() == null)
				continue;			
			response.add(new Laboratorio(dto.getNome(), dto.getEndereco()));
		}

		return laboratorioService.saveAll(response);
	}
	
	@GetMapping("/laboratorios")
	@ApiOperation(value = "Retorna todos os laboratórios ativos.", httpMethod = "GET", response = Laboratorio.class)
	public Set<Laboratorio> listarAtivos() {
		return laboratorioService.buscarTodosAtivo();
	}
	
	@GetMapping("/laboratorios/{nomeExame}")
	@ApiOperation(value = "Retorna todos os laboratórios associados ao exame.", httpMethod = "GET", response = List.class)
	public List<Laboratorio> buscarPorNomeExame(@PathVariable String nomeExame) throws ResourceNotFoundException {
		if (nomeExame == null)
			throw new ResourceNotFoundException(Mensagens.ERRO_CAMPOS_NAO_PREENCHIDOS);
		return laboratorioService.buscarPorNomeExame(nomeExame);
	}
	
	@PutMapping("/laboratorios")
	@ApiOperation(value = "Atualiza um laboratório.", httpMethod = "PUT", response = Laboratorio.class)
	public Laboratorio atualizar(@RequestBody Laboratorio laboratorio) throws ResourceNotFoundException {
		if (laboratorioService.update(laboratorio) == null)
			throw new ResourceNotFoundException(Mensagens.ERRO_LABORATORIO_NAO_ENCONTRADO);

		return laboratorio;
	}
	
	@PutMapping("/laboratoriosEmLote")
	@ApiOperation(value = "Atualiza em lote laboratórios.", httpMethod = "PUT", response = List.class)
	public List<Laboratorio> atualizarEmLote(@Valid @RequestBody List<Laboratorio> lista) {
		
		lista.removeIf(i -> i.getId() == null || i.getNome() == null || i.getEndereco() == null);

		return laboratorioService.updateAll(lista);
	}
	
	@DeleteMapping("/laboratorios/{id}")
	@ApiOperation(value = "Deleta o laboratório do ID informado.", httpMethod = "DELETE", response = Laboratorio.class)
	public Map<String, Boolean> deletar(@PathVariable Long id) throws ResourceNotFoundException {
		Laboratorio laboratorio = laboratorioService.findById(id);

		if (laboratorio == null)
			throw new ResourceNotFoundException(String.format(Mensagens.ERRO_ID_EXAME_NAO_ENCONTRADO, id));

		laboratorioService.delete(id);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deletado", Boolean.TRUE);
		return response;
	}	
	
	@DeleteMapping("/laboratoriosEmLote")
	@ApiOperation(value = "Deleta toodos os laboratórios.", httpMethod = "DELETE", response = Map.class)
	public Map<String, Boolean> deletarEmLote() {
		laboratorioService.deleteAll();
		Map<String, Boolean> response = new HashMap<>();
		response.put("deletado", Boolean.TRUE);
		return response;
	}	

}
